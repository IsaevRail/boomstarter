'use strict';

var app = {

  init: function () {

    app.copy.init();
    app.mfp.init();


  },

  copy: {

    init: function () {

      app.copy.change_color();
      app.copy.clipboard();

    },

    change_color: function () {

      var select = $('.copy__select');
      var checkbox = select.find('input[type="radio"]');
      var btn = $('.copy__btn');
      var btn_white = $('.copy__btn.copy__white');
      var btn_black = $('.copy__btn.copy__black');

      checkbox.on('change', function () {

        var $this = $(this);
        var label = $this.closest('label');

        if ( label.hasClass('white') ) {

          btn.removeClass('active show');
          btn_white.addClass('active');
          setTimeout(function () {
            btn_white.addClass('show');
          }, 10);

        } else if ( label.hasClass('black') ) {

          btn.removeClass('active show');
          btn_black.addClass('active');
          setTimeout(function () {
            btn_black.addClass('show');
          }, 10);

        }

      });

    },

    clipboard: function () {

      var btn = $('.btn-clipboard');

      btn.on('click', function (e) {
        e.preventDefault();

        var copy_btn = $('.copy__btn');

        copy_btn.each(function () {

          if ( $(this).hasClass('active') ) {

            var html = $(this).html();

            copyToClipboard(html);

            btn.text('Код скопирован');
            btn.addClass('active');

            setTimeout(function () {
              btn.text('Скопировать код');
              btn.removeClass('active');
            }, 3000);

          }
        })




      });

      const copyToClipboard = str => {
        const el = document.createElement('textarea');  // Create a <textarea> element
        el.value = str;                                 // Set its value to the string that you want copied
        el.setAttribute('readonly', '');                // Make it readonly to be tamper-proof
        el.style.position = 'absolute';
        el.style.left = '-9999px';                      // Move outside the screen to make it invisible
        document.body.appendChild(el);                  // Append the <textarea> element to the HTML document
        const selected =
            document.getSelection().rangeCount > 0        // Check if there is any content selected previously
                ? document.getSelection().getRangeAt(0)     // Store selection if found
                : false;                                    // Mark as false to know no selection existed before
        el.select();                                    // Select the <textarea> content
        document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)
        document.body.removeChild(el);                  // Remove the <textarea> element
        if (selected) {                                 // If a selection existed before copying
          document.getSelection().removeAllRanges();    // Unselect everything on the HTML document
          document.getSelection().addRange(selected);   // Restore the original selection
        }
      };

    }

  },

  mfp: {

    init: function () {

      $('.open-modal').magnificPopup({
        removalDelay: 300,
        mainClass: 'mfp-fade'
      });

    }

  }

};

$(document).ready(app.init());


























